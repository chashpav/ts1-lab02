package cz.cvut.fel.ts1;

/**
 *
 * @author chashchyn
 */

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.math.BigInteger;

public class CounterTest {

    @Test
    public void factorialPositive1(){
        BigInteger actual = Main.factorialCount(BigInteger.TEN);
        BigInteger expected = new BigInteger("3628800");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorialPositive2(){
        BigInteger actual = Main.factorialCount(BigInteger.ZERO);
        BigInteger expected = new BigInteger("1");
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void factorialPositive3(){
        BigInteger actual = Main.factorialCount(BigInteger.ONE);
        BigInteger expected = new BigInteger("1");
        Assertions.assertEquals(expected, actual);
    }
}
