package cz.cvut.fel.ts1;

/**
 *
 * @author chashchyn
 */

import java.math.BigInteger;

public class Main {
    public static void main () {
        java.util.Scanner s = new java.util.Scanner(System.in);
        System.out.println("Enter a number:");

        BigInteger num = s.nextBigInteger();

        if (num.equals(0)) {
            System.out.println("The factorial of 0 is 1");                                                               //+ num);
        if (num.compareTo(BigInteger.ZERO) > 0) {
            System.out.println("Does not exist");
            }
        } else {
            System.out.printf("The factorial of 0 is " + num + "\n");
        }
    }

    public static BigInteger factorialCount (BigInteger n){
        if (n.equals(BigInteger.ZERO)) return BigInteger.ZERO;
        BigInteger result = n;

        for (BigInteger i = BigInteger.ONE; i.compareTo(n) < 0; i = i.add(BigInteger.ONE)){
            result = result.multiply(n.subtract(i));
        }
        return result;
    }
}
